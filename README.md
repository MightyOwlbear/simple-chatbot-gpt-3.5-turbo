Functional Python code to create a gpt-3.5-turbo chatbot using OpenAI's API.

### Includes
* openai-sample-code.py - send a single prompt and recieve a response via the API
* simple-chatbot.py - a basic two-way chatbot. Previous prompts and responses are appended to the messages array. 
* simple-chatbot-with-intro.py - as above, but a response to the initial prompt is requested, allowing the bot to introduce itself.
* simple-chatbot-with-intro-and-outro.py - as above but with keyword detection to exit cleanly upon saying Goodbye or Farewell

### Limitations
* Requires internet connection.
* OpenAI's models are proprietary and require both an account and an OpenAI API key from https://platform.openai.com/account/api-keys
* This bot will crash when its messages array reaches gpt-3.5-turbo's maximum limit of 4096 tokens (~3,000 words in English).

### To install required Python library dependencies
pip install openai 

### Further useful examples and reference scripts:
* https://github.com/openai/openai-cookbook/blob/main/examples/How_to_format_inputs_to_ChatGPT_models.ipynb