# Learn more at https://github.com/openai/openai-cookbook/blob/main/examples/How_to_format_inputs_to_ChatGPT_models.ipynb

# import the OpenAI Python library for calling the OpenAI API
import openai

# Your openAI API key goes here:
openai.api_key = "PasteYourKeyHere"

# Example OpenAI Python library request - defines the model we wish to use, and uses the ChatCompletion routine to send a prompt.
MODEL = "gpt-3.5-turbo"
response = openai.ChatCompletion.create(
    model=MODEL,
    messages=[
        {"role": "user", "content": "Pretend that you are a sentient Raspberry Pi computer who feels really positive about itself. In that role, explain why Raspberry Pi computers are great."},
    ],
    temperature=0.2
)

# extract and print the response
print(response['choices'][0]['message']['content'])

# exit when finished 
exit()
